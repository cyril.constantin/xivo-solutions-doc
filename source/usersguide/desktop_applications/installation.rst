.. _desktop-application-installation:

************
Installation
************

The **UC Assistant** and **CC Agent** are available as desktop application through Electron packaging, to be able to use these applications in a standalone executable you need to deploy this container first on client machine.

Windows (64bits)
================

There are two ways for downloading the latest version of the Desktop assistant on your environment. 

- You can either do it by clicking the Windows button on the login page from your browser, on the CCAgent or UCAssistant applications. 

.. figure:: download-desktop.png

.. note::

          You can disable those download buttons from the login page by adding
          SHOW_APP_DOWNLOAD to your XiVO CC custom.env and setting it to false


- Or you can download it by opening the following url from your computer:: 
   
   https://<xivo_uc_cc>/install/win64

and then start the downloaded program.

.. note::

          If you have a secured installation (using https/wss) the port can be omitted as the default
          port is already 443, generally speaking use the uc-assistant URL followed by `/install/win64`.


Linux (Debian 64bits)
=====================

To install the latest version, you need to add a repository linked to the XiVO UC/CC CTI Server host. Create the file :file:`/etc/apt/sources.list.d/xivo-desktop-assistant.list` containing the following line:

.. code-block:: bash

   deb [trusted=yes] https://<xivo_uc_cc>/updates/debian jessie contrib


Then run

.. code-block:: bash

   sudo apt-get update
   sudo apt-get install xivo-desktop-assistant

.. note:: This repository is currently not signed at all.
