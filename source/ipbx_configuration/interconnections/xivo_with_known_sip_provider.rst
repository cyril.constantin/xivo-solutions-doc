.. index:: interconnections

*******************************************
Interconnect XiVO with a known SIP Provider
*******************************************

Connection to global telephony network can be configured automatically in this version of XiVO.
For instructions how to configure it manually, see :ref:`voip_provider`.

Requirements
============

This is a premium feature that is not included in XiVO and is not freely available.
To enable it, please contact XiVO.Solutions customer support.

Overview
========

The SIP provider configuration can be applied from the SIP provider page.

.. figure:: images/provider_menu.png
   :scale: 85%

On this page, you will not see all the
settings that will be applied, but only those that must be personalised.

.. figure:: images/provider.png
   :scale: 85%

These settings will apply when you save the form:

* SIP trunk with the settings required by the provider will be created
* Data entered to the form will be included in the trunk
* If it is required by the provider, other XiVO settings will be changed.
  These settings can't be reverted by removing the trunk

Installation
============

* Open SIP Provider page from menu :menuselection:`Services --> IPBX --> Trunk management --> SIP Provider`
* Choose provider
* Fill up the form
* Save
* Some :ref:`manual steps <voip_provider_outcall>` may be required to complete the configuration
