.. _caller_number_normalization:

***************************
Caller Number Normalization
***************************

Providers can send the **caller** number in various formats (e.g. for french national number you could receive 0123456789 or +33123456789 or even 33123456789). 
You can change the caller number to adapt to your need. For examples:

* you may want to always display incoming numbers in E.164 format (e.g. +33123456789)
* or, if you use a prefix to dial outgoing numbers (like a 0), you should add a 0 to the received caller number so you can redial it.

This caller number normalization is done via the :ref:`xivo_in_callerid.conf` file.

.. important:: Before modifying this file, you must know:

  * that any caller number modification **will break** the :ref:`agent_customer_history`,
  * and that the :ref:`reverse_lookup` is done with the caller number **after** it has been modified
    through the rules of this :file:`xivo_in_callerid.conf` file.

  **Therefore**, if you need to have the :ref:`agent_customer_history` (used in *CC Agent* and *Switchboard* applications),
  then you must void all the rules in this file (i.e. remove the content of the ``add`` and ``strip`` lines so the caller number is not changed).
  *But then* you must take care that, if you also use the :ref:`reverse_lookup`, the phone numbers in your directory are stored in
  the same format than received from the provider (i.e. if your provider sends *+33123456789* the phone number must be stored in the
  directory as *+33123456789*). 

Default rules
=============

By default the following rules are applied


National number with missing prefix
-----------------------------------

* rule: if caller number does not start by ``0`` and is 9 digits long
* action: add a leading ``0``
* example: ``123456789`` is normalized to ``0123456789``


French national number E.164 format
-----------------------------------

* rule: if caller number starts with ``+33`` and is followed by 9 digits
* action: replace ``+33`` with ``0``
* example: ``+33123456789`` is normalized to ``0123456789``


International number E.164 format
---------------------------------

* rule: if caller number starts with ``+`` followed by at least 4 digits
* action: replace ``+`` with ``00``
* example: ``33123456789`` is normalized to ``0033123456789``


International number with missing prefix
----------------------------------------

* rule: if caller number does not start by ``0`` and is at least 11 digits long
* action: add a leading ``00``
* example: ``33123456789`` is normalized to ``0033123456789``

