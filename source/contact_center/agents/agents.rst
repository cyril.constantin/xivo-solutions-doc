******
Agents
******

   *A call center agent is the person who handles incoming or outgoing customer
   calls for a business. A call center agent might handle account inquiries, 
   customer complaints or support issues. Other names for a call center agent 
   include customer service representative (CSR), telephone sales or service 
   representative (TSR), attendant, associate, operator, account executive 
   or team member.*

   -- SearchCRM

In this respect, agents in XiVO have no fixed line and can login from any registered device.

.. contents:: :local:


Creating agents
===============

* Create a user with a SIP line and a provisioned device
* Select agent group in :menuselection:`Services --> Call Center --> Agents`
* :menuselection:`Add an agent` from the dropdown menu

*General* tab:

  * :guilabel:`Context` must be the same as the associated user's line context.
    If you change context of an existing agent, the agent must relog to apply the change.

*Users* tab:

  * Associate the agent with the user (with SIP line and a provisioned device)


Agent with external line
************************

XiVO system agents can be external to the system, the agent can use it's personal PSTN line, mobile phone or a terminal
connected to some other PBX system. We call these remote lines external line. The same agent can login to a standard
line, or to an external line. The choice is done via the line number on the login page.

Creating agents with external line
----------------------------------

Agent settings are the same, the only difference is in the line which is used by the agent. You must create a user with
a custom line:

* Start by creating a standard user, when creating a line pick up a line number and choose Customized line protocol.

.. figure:: user_with_custom_line_creation.png
    :scale: 90%

* Then save the user, go to lines listing and edit the created custom line.

.. figure:: custom_line_editing.png

* Replace the line Interface by a string with following format:

  Local/EXTERNAL_LINE_NUMBER@default/n

where EXTERNAL_LINE_NUMBER@default is the number and context which can be used to join the remote phone, for a french
mobile phone it would usually be `0xxxxxxxxx@default`.

Usage
-----

The agent has to login using the custom line, the standard ccagent features are available. However, due to external line
some phone control features are not available - the agent can't answer, put on hold or transfer calls from the ccagent
interface. On the XiVO side, please ensure that the call distributed to the agent is canceled if the agent doesn't answer
before the call is answered by for example the voicemail.


Managing agents in groups
=========================

Every agent must be in exactly one group. Groups can be used to:

* Assign group of agents to a queue in :ref:`CCmanager <ccmanager>` Group View
* Filter agents by group in :ref:`CCmanager <ccmanager>` Agent View

Groups can be created from :menuselection:`Services --> Call Center --> Agents --> Add a group`.
There is a limit of 120 groups.


Assigning agents to queues
==========================

There are three ways to assign agents to queues:

* Assign agents one by one in :ref:`CCmanager <ccmanager>` Global View
* Assign one agent to any selection of queues: :menuselection:`Services --> Call Center --> Agents --> Edit --> Queues`
* Assign any selection of agents to a queue: :menuselection:`Services --> Call Center --> Queues --> Edit --> Members`


Global settings for all agents
==============================

This option can be set in :menuselection:`Services --> IPBX --> General settings --> Advanced --> Agent`:

* Autodisconnect if channel unavailable
