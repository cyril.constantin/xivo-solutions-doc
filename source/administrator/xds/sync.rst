.. _xds_file_sync:

********************
File Synchronization
********************

When using XDS architecture one should activate the *File Synchronization* between *XiVO Main* and *MDS*.

Installation
============

#. One must initialize the configuration on *XiVO Main*: refer to :ref:`xds_xivo-rsync-init` section in Installation page.

   .. note:: What does the initialization do ? The ``xivo-xds-sync -i`` is to be run on XiVO Main and will:
  
      #. generate a ssh key pair: ``~/.ssh/rsync_xds`` and ``~/.ssh/rsync_xds.pub``
      #. copy the public key to ``/usr/share/xivo-certs/``
      #. make the public key available at ``https://XIVO_HOST/ssh-key/rsync_xds.pub``
      #. create cron job ``/etc/cron.d/xivo-xds-sync`` to schedule the synchronization

#. The configuration on MDS is automatic when installing a new MDS.

   .. note:: What does the MDS install script do ? When running the ``mds_install.sh`` script it will:

      #. get the *XiVO Main* pub key from ``https://XIVO_HOST/ssh-key/rsync_xds.pub``
      #. and put it in the ssh *authorized_keys*


What is synchronized ?
======================

Synchronized dirs
-----------------

* /etc/asterisk/extensions_extra.d
* /var/lib/xivo/moh
* /var/lib/xivo/sounds

Synchronization period
----------------------

Every **15 min**

Exclusion
---------

* Files in the above listed folders prefixed with ``xds_override`` **won't** be synchronized.
  
  .. note:: For example if you create on the *XiVO Main* the file
    :file:`/etc/asterisk/extensions_extra.d/xds_override_mydialplan.conf`,
    it won't be synchronized to the other MDS.

* Files in :file:`/etc/asterisk/extensions_extra.d/` are not overriden on MDS. It means that if you create on **MDS1** a file in :file:`/etc/asterisk/extensions_extra.d/` it will remain on MDS1 (as long as the filename is not the same as one on the *XiVO Main*).

Destination
-----------

All files are synchronized to every configured MDS (MDS as defined in :menuselection:`Configuration --> Management --> Media Servers`).

.. note:: And therefore uses the MDS so-called VoIP IP address.


Reload
------

Dialplan (`dialplan reload`) and MOH (`moh reload`) are reloaded on each MDS after each synchronization.


Logging and Troubleshooting
===========================


All the script logs are logged in :file:`/var/log/syslog`. All logs are prefixed with ``xivo-xds-sync`` prefix.
You can run the following command to get the logs::

  grep xivo-xds-sync /var/log/syslog


To see what the script is doing/will do you can run it in *dry* mode::

  xivo-xds-sync -n
