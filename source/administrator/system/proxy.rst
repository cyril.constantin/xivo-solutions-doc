.. _system_proxy:

*******************
Proxy Configuration
*******************

If you use XiVO behind an HTTP proxy, you must do a couple of manipulations for
it to work correctly.

.. warning:: We do not recomend to use ``http_proxy`` environment variable. It may
     break some services.
     Instead you should configure the proxy on a per service basis as described
     below.


System Applications
===================

Installation and upgrade operations use different tools for which the proxy must be configured if any.


apt
---

.. important:: This is needed because apt is used for installation and upgrade

Create the :file:`/etc/apt/apt.conf.d/90proxy` file with the following content::

   Acquire::http::Proxy "http://domain\username:password@proxyip:proxyport";

curl
----

.. important:: This is needed because ``curl`` is used during installation and upgrade

Create the :file:`~/.curlrc` file with the following content::

   proxy = http://proxyip:proxyport
   proxy-user = "username:password"


docker
------

.. important:: This is needed because docker images will be downloaded during installation or upgrade

When upgrading or installing XiVO it will attempt to download docker images.
For the proxy configuration, you need to create a systemd configuration file.
See Docker documentation: https://docs.docker.com/config/daemon/systemd/#httphttps-proxy


wget
----

.. important:: This step is needed because this tool is used by xivo-upgrade script and install scripts

Create the :file:`~/.wgetrc` file with the following content::

   use_proxy=yes
   http_proxy=http://username:password@proxyip:proxyport


XiVO Services
=============

Several XiVO services needs also the proxy to be configured, if any.


dhcp-update
-----------

.. important:: This is needed if you use the DHCP server of the XiVO. Otherwise the DHCP configuration won't be correct. It must be set before the wizard is run.

Proxy information is set via the :file:`/etc/xivo/dhcpd-update.conf` file.

Edit the file and look for the ``[proxy]`` section.


provd
-----

.. note:: This is needed to download plugins

Proxy information is set via the :menuselection:`Configuration --> Provisioning --> General`
page.


xivo-fetchfw
------------

.. note:: This is needed to download firmwares

Proxy information is set via the :file:`/etc/xivo/xivo-fetchfw.conf` file.

Edit the file and look for the ``[proxy]`` section.
