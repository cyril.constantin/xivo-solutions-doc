.. _web_socket_api:

**************
Web Socket API
**************

The xivo solutions web socket API enables you to integrate enterprise communication functions to your business application.
It exposes Cti functions using javascript methods calls and web socket events.

You may add your own handlers for your application to react to telephony / contact center events.

This API is using websockets_, and therefore needs a modern browser supporting them (firefox_, chrome ...)

.. _firefox: https://www.mozilla.org/en-US/firefox/desktop/
.. _websockets: http://en.wikipedia.org/wiki/WebSocket


.. toctree::
   :maxdepth: 3

   developers
   generic
   user
   phone
   agent
   queue
   history
   conferences
   chat
   webrtc
