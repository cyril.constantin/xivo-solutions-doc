************
API Security
************

.. _api_shared_token:

Shared token
============

Accessing the :ref:`configmgt_api` or the :ref:`recording_api` requires a token. This token is used by the Xuc process but can also be used to access API from custom server processes. This token must remain on the server and should not be leaked as it would allow API access without restrictions.

To define or change it, you can add or update the value of the ``PLAY_AUTH_TOKEN`` environment variable defined in the ``custom.env`` of all the following docker environment (where applicable):

:XiVO: :file:`/etc/docker/xivo/custom.env`
:XiVO CC: :file:`/etc/docker/compose/custom.env`
:XiVO UC-Addon: :file:`/etc/docker/compose/custom.env`

.. warning::
            * Remind that the value, if defined, must be the same across all XiVO components.
            * When the XiVO CC is splitted on separate server, the value must also be defined consistently.

For more information, see also the :ref:`components_configuration` and :ref:`multi-server_installation` sections.

.. _xuc_credentials:

Unified Communication Credentials
=================================

Depending on the authentication mode you will require different kind of informations to access the :ref:`xuc_api`. To simplify and unify authentication, API access is based on a custom token that can be used one of the following way, depending of the API you access:

:X-Auth-Token Header: Adding a HTTP header with a valid token
:Query string parameter: Adding a `token` parameter with a valid token in the query string part of the API url.

The following sections explain how to get a valid token or trade an external token to a :ref:`xuc_api` token.


Login based access
------------------
You can use any valid user (see :ref:`users_configuration`) with the following properties:

* CTI Login enabled
* Login
* Password
* Profile set to any valid value

With these informations you can then use the :ref:`rest_authentication` API to get a :ref:`xuc_api` token.

Kerberos token
--------------
A kerberos ticket is normally obtained automatically by the browser when accessing a resource requiring access privilge. So in most case a simple HTTP call to the :ref:`kerberos_authentication` API will get you a :ref:`xuc_api` token.

CAS token
---------
CAS token can be obtained by authenticating on a CAS server for a given service. Once you have a token, you can trade it to a :ref:`xuc_api` token using the :ref:`cas_authentication` API.

OpenID Token
------------

OpenID token can be obtained by authenticating on an OpenID server for a given client. Once you have a token, you can trade it to a :ref:`xuc_api` token using the  :ref:`openid_authentication` API.



