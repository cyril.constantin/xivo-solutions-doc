*******************
Exposing Mattermost
*******************

.. warning:: Experimental Feature

    * this is Work In Progress
    * this is not for production use
    * no support will be provided for this feature


Description
===========

The goal of this feature is to be able for users to use Mattermost embedded in XiVO UC/CC solutions.


Work in Progress
================

.. |checked| raw:: html

    <input checked=""  disabled="" type="checkbox">

.. |unchecked| raw:: html

    <input disabled="" type="checkbox">

* |checked| Expose Mattermost from docker configuration
* |checked| Have a XiVO theme installed automatically
* |unchecked| Be able to be auto-logged Cti users inside Mattermost once connected to UC Assistant or Switchboard
* |unchecked| Be able to receive chat messages on any chat client connected (UC or Mattermost)


Configuration
=============

To enable the access to embedded *Mattermost* you need to have as prerequisite ``xivo-chat-backend`` package installed (see :ref:`install_chat_backend`).

* Allow port 8000 to be exposed in docker

  Edit your docker compose file (:file:`/etc/docker/compose/docker-xivo-chat-backend.yml`) and change the configuration of the mattermost container:

  ::

     mattermost:
       [...]
       ports:
       - 8000:8000
       [...]

* Launch ``xivocc-dcomp up -d`` to make the port accessible
* Run script ``/var/lib/xivo-chat-backend/scripts/xivo-mattermost-theme-install.sh`` to install XiVO theme for users
* (**Option**) You may want to disable Chat in UC assistant if not needed anymore - see :ref:`webassistant_disable_chat` section

The Mattermost is then available `http://xivo_uc:8000`.

.. note:: You can can connect as `admin` with associated password found in :file:`/etc/docker/compose/custom.env` file.

Limitations
===========

* Currently there is no integration at all between XiVO users and Mattermost, if you want to use Mattermost as a chat solution, it is recommended to create your own team and create different users in Mattermost.
