.. _xivosolutions_release:

*************
Release Notes
*************

.. _gaia_release:

Gaia (2021.??)
==============

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Freya (2020.18).

New Features
------------

**Assistants**

* *Common features*

  * It is possible to customize email subject and body when clicking on email link in search results - see :ref:`UC Assistant Search <uc_assistant_search>` or :ref:`CC Agent/Switchboard Search <agent_search>` and :ref:`how to configure it <email_template_configuration>`
* *UC Assistant*

  * When in conference, organizer can invite a user by typing its name or phone number in the search bar - see :ref:`invite to conference in Attendee actions <uc-assistant_conferences_actions>`
* *WebRTC*

  * If user's microphone seems to be turned off during the beginning of the call, a message appears to inform him (so he can fix his setup) - see :ref:`webrtc_sound_detection`.
  * There is live feedbacks of the ongoing call audio quality in the form of a blinking color on the call line - see :ref:`webrtc_quality_detection`.
  * If a network or server issue can cause the ongoing call audio to be impacted, a message appears to inform the user - see :ref:`webrtc_quality_detection`.

**API**

* New conference API - see detail in :ref:`conference_methods`:

  * to Close a conference,
  * to Deafen/Undeafen a given participant,
  * to Invite someone (a xivo user or an extenion) to the current conference

**XiVO PBX**

* *WebRTC Line*:

  * WebRTC line are now configured by default to use ``opus,alaw,vp8`` codecs
  * Admin can customize codecs for WebRTC line (previously it was not taken into account)
* In :menuselection:`IPBX --> General --> SIP Protocol`, there is a new field :guilabel:`XiVO Edge FQDN` to configure the link to `XiVO Edge`.

  .. warning:: If this field is not empty, the XiVO Edge server **must be running** otherwise it will break all WebRTC communications


**Asterisk**

* Version was upgraded to Asterisk 18.2.1
* Added support for opus codec

**XDS**

* File synchronization of custom dialplans, music on hold and custom sounds from XiVO Main to all media servers - see :ref:`xds_file_sync` page.

**System**

* Support OpenID Connect SSO Authentication - see :ref:`oidc-sso-configuration`
* You can now disable the :ref:`XiVO based authentication <disabling-xivo-authentication>` to enforce another authentication mechanism.
* Upgrade docker-ce to 19.03.13
* Upgrade docker-compose to 1.27.4
* Upgrade ELK stack (Elastic, Logstash and Kibana) to 7.10.0
* Upgrade nginx to 1.19.5
* Upgrade electron to 11.0.2

Behavior Changes
----------------

**Assistants**

* *Common features*

  * By default, the remember me feature will only work for 1 hour after leaving the application. Passed that delay you will need to retype your password. You can revert this behaviour at your own risks by changing the :ref:`credentials_validity_period`.
* *UC Assistant*

  * When in conference and typing a phone number in the searchbar, a dropdown menu will show with 3 options :
    Call, invite to conference or search.
* *CC Agent*

  * (*for version below Freya.05*) History: :ref:`agent_agent_history` is now limited to the last 7 days: it displays the last 20 calls for the last 7 days period
    (previously it was displaying the last 20 calls with no period limit - which could overload the Reporting Server).
* *WebRTC*

  * Default codecs for WebRTC lines (also WebRTC line of UA user) was changed to ``opus,alaw,vp8``
    (beforehand it was a combination of the SIP codec defined in the General settings of the SIP protocol plus *ulaw,vp8*)
  * When leaving the web/desktop-assistant (or agent) while having an ongoing webrtc call, and if you confirm, the call will be hung up.


**XiVO PBX**

* *WebRTC Line*: if you customize the line's codec (while editing a line in the :menuselection:`Services --> IPBX --> IPBX Settings --> Lines` menu):

  * Phone and WebRTC lines will use only the chosen codecs
  * for Unique Account line, codec customization only applies to the WebRTC line
* In :menuselection:`IPBX --> General --> SIP Protocol`, the :guilabel:`STUN Address` field has been renamed to :guilabel:`XiVO Edge FQDN`. 
  When upgrading from a version prior to Gaia.00, this field will be cleared.

  .. warning:: If this field is not empty, the XiVO Edge server **must be running** otherwise it will break all WebRTC communications.
   
**API**

* After installation or upgrade, a :ref:`api_shared_token` is generated to access the :ref:`configmgt_api` or the :ref:`recording_api`. Customers using these API need to fetch the correct value from the configuration file or change it following the :ref:`api_shared_token` documentation section.
* For security reasons, when using the api to :ref:`obtain an authentication token<rest_authentication>`, you will now receive a generic InvalidCredentials error when username or password is incorrect instead of a detailled error.
* The api to :ref:`obtain an authentication token<rest_authentication>` now return the time to leave in seconds of the credentials.


.. _gaia_release_deprecations:

Deprecations
------------

This release deprecates:

* `LTS Aldebaran (2018.05) <https://documentation.xivo.solutions/en/2018.05/>`_: after 3 years of support this version is no longer supported.
  No bug fixes, no security update will be provided for this release.

Upgrade
-------

.. _upgrade_lts_manual_steps:

**Manual steps for LTS upgrade**

.. warning:: **Don't forget** to read carefully the specific steps to upgrade from another LTS version

.. toctree::
   :maxdepth: 1

   upgrade_from_five_to_polaris
   upgrade_from_polaris_to_aldebaran
   upgrade_from_aldebaran_to_borealis
   upgrade_from_borealis_to_callisto
   upgrade_from_callisto_to_deneb
   upgrade_from_deneb_to_electra
   upgrade_from_electra_to_freya
   upgrade_from_freya_to_gaia


**Generic upgrade procedure**

Then, follow the generic upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Gaia Bugfixes Versions
======================

Components version table
------------------------


Gaia Intermediate Versions
==========================

.. toctree::
   :maxdepth: 2

   gaia_iv
