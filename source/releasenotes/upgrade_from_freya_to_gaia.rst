*********************
Upgrade Freya to Gaia
*********************

In this section are listed the manual steps to do when migrating from Freya to Gaia.

.. warning:: Known Upgrade Limitations: as of 2021.06 release these are the known limitations for upgrade.

    * Following upgrade to Asterisk 18, libsccp is available for this asterisk version but was not tested with this Asterisk 18

.. contents::


Before Upgrade
==============

On XiVO PBX
-----------

  .. note:: In :menuselection:`IPBX --> General --> SIP Protocol`, the field :guilabel:`XiVO Edge FQDN` will be emptied.

On XiVO CC
----------

* Reporting: During upgrade all **Kibana configuration** (including the dashboard) will be lost (it is stored in *elasticsearch* container).
  You **MUST** :ref:`backup Kibana configuration <xivocc_backup_kibana>` before the upgrade.


On MDS
------


After Upgrade
=============

On XiVO PBX
-----------

* **XDS**

  * **File synchronization**: On XiVO Main, initialize the :ref:`xds_xivo-rsync-init`


On XiVO CC/UC
-------------

* Docker: you **MUST** upgrade ``docker-ce``.

    * Check that you correctly have upgraded ``xivo-dist``:

     .. code-block:: bash

      # Update APT sources
      apt-get update
      # Install xivo-dist (to prepare docker installation)
      apt-get install xivo-dist

    * Update ``docker-ce``:

     .. warning:: This step (upgrading docker) will **restart** the containers

     .. code-block:: bash

      apt-get install docker-ce

On XiVO CC only
^^^^^^^^^^^^^^^

* Reporting:

  * :ref:`Restore Kibana configuration <xivocc_restore_kibana>`.
  * The last 7 days of data will be re-replicated to Elasticsearch, see :ref:`totem_data_flow`.
    It may take some time if you have a huge amount of calls per week (more than 1 hour if you have 2 million of queue_log per week).


On MDS
------

* **File synchronization**: after having initialized the file synchronization on Main (see above), execute the following commands **on each MDS** to get the public ssh key setup for rsync access from XiVO Main:

     .. code-block:: bash

      XIVO_HOST=$(grep XIVO_HOST /etc/docker/mds/custom.env | awk -F "=" '/1/ {print $2}')
      mkdir -p /root/.ssh
      wget https://$XIVO_HOST/ssh-key --no-check-certificate -O /root/.ssh/rsync_xds.pub
      cat /root/.ssh/rsync_xds.pub >> /root/.ssh/authorized_keys
      apt install -y rsync
