*******************************
XiVO Gaia Intermediate Versions
*******************************

2021.06
=======

Consult the `2021.06 Roadmap <https://projects.xivo.solutions/versions/244>`_.

Components updated: **asterisk**, **config-mgt**, **play-authentication**, **xivo-confgend**, **xivo-config**, **xivo-edge**, **xivo-libsccp**, **xivo-web-interface**, **xivo-webi-nginx**, **xivocc-installer**, **xivoxc-nginx**, **xucmgt**, **xucserver**

**Asterisk**

* `#3733 <https://projects.xivo.solutions/issues/3733>`_ - Prepare Asterisk 18 with XiVO patches
* `#3821 <https://projects.xivo.solutions/issues/3821>`_ - Asterisk 18 - Test non-regression on device synchronization
* `#3823 <https://projects.xivo.solutions/issues/3823>`_ - Asterisk 18 - Test non-regression on transfer towards E.164 number format
* `#3824 <https://projects.xivo.solutions/issues/3824>`_ - Asterisk 18 - Test non-regression of asterisk 11 compatibility patches
* `#3874 <https://projects.xivo.solutions/issues/3874>`_ - Asterisk 18 - CEL - app list in cel.conf is no longer case insensitive
* `#3875 <https://projects.xivo.solutions/issues/3875>`_ - Asterisk 18 - Retrieving a call from queue via Bridge application doesn't should raise a QueueCallerLeave AMI event (switchboard related)
* `#3937 <https://projects.xivo.solutions/issues/3937>`_ - Update libsccp for asterisk 18

**CCAgent**

* `#3879 <https://projects.xivo.solutions/issues/3879>`_ - Agent unable to mute himself while in conference
* `#3880 <https://projects.xivo.solutions/issues/3880>`_ - Display issue when receiving call from external number in CCAgent

**Config mgt**

* `#3943 <https://projects.xivo.solutions/issues/3943>`_ - CTI authent: create new endpoint to check single user

**Security**

* `#2452 <https://projects.xivo.solutions/issues/2452>`_ - [S] Disable TLSv1.X <= 1.2

**Switchboard**

* `#3878 <https://projects.xivo.solutions/issues/3878>`_ - Unable to hangup ringing call after re-logging for webrtc agent

  .. important:: **Behavior change** When leaving the web/desktop-assistant (or agent) while having an ongoing webrtc call, and if you confirm, the call will be hung up.


**Web Assistant**

* `#3877 <https://projects.xivo.solutions/issues/3877>`_ - UC Assistant - First call after login rings like a second call 
* `#3924 <https://projects.xivo.solutions/issues/3924>`_ - Kerberos - authentication after standby or change of networks (cable / wifi)

**WebRTC**

* `#3696 <https://projects.xivo.solutions/issues/3696>`_ - As a XiVO Admin I want to be able to find in logs poor webrtc communication
* `#3896 <https://projects.xivo.solutions/issues/3896>`_ - Shorten CTI token and include renewal mechanism

  .. important:: **Behavior change** The remember me feature will only work for 1 hour after leaving the application. Passed that delay you will need to retype your password. You can revert this behaviour at your own risks by changing the :ref:`credentials_validity_period`.

* `#3897 <https://projects.xivo.solutions/issues/3897>`_ - Be able to get TURN info and credentials before initializing the webrtc stack with renewal mechanism
* `#3902 <https://projects.xivo.solutions/issues/3902>`_ - Security protection in Kamailio routing SIP
* `#3906 <https://projects.xivo.solutions/issues/3906>`_ - Be able to use TURN server for asterisk
* `#3923 <https://projects.xivo.solutions/issues/3923>`_ - Edge - Clarify  XiVO Edge configuration in Webi
* `#3929 <https://projects.xivo.solutions/issues/3929>`_ - Brute force auth protection in Kamailio
* `#3931 <https://projects.xivo.solutions/issues/3931>`_ - [C] - Video of called party is seen before he answers
* `#3949 <https://projects.xivo.solutions/issues/3949>`_ - When calling via Edge server, caller does not receive ACK

**XUC Server**

* `#3910 <https://projects.xivo.solutions/issues/3910>`_ - CTI authent: do not return to the client (frontend) whether it was the username or password that was wrong

  .. important:: **Behavior change** For security reasons, when using the api to :ref:`obtain an authentication token<rest_authentication>`, you will now receive a generic InvalidCredentials error when username or password is incorrect instead of a detailled error.

* `#3940 <https://projects.xivo.solutions/issues/3940>`_ - CTI authent: unify authentication methods - xuc

  .. important:: **Behavior change** The definition of authentication methods in project configuration file determines which method is going to be used. If the method is not defined it will be disabled.


**XiVO PBX**

* `#3798 <https://projects.xivo.solutions/issues/3798>`_ - UA - Editing user, selection Web RTC prior to Unique Account for line type breaks link between device & user
* `#3909 <https://projects.xivo.solutions/issues/3909>`_ - CTI authent: enhance the CTI password policy


2021.05
=======

Consult the `2021.05 Roadmap <https://projects.xivo.solutions/versions/242>`_.

Components updated: **config-mgt**, **recording-server**, **xivo-confgend**, **xivo-edge**, **xivo-web-interface**, **xucmgt**, **xucserver**

**Recording**

* `#3855 <https://projects.xivo.solutions/issues/3855>`_ - Purge date is wrong in logs

**Web Assistant**

* `#3881 <https://projects.xivo.solutions/issues/3881>`_ - Display call control right after dialing a number
* `#3888 <https://projects.xivo.solutions/issues/3888>`_ - Duplicate number on outgoing call
* `#3899 <https://projects.xivo.solutions/issues/3899>`_ - [C] - Forbid directory lookup with no value

  .. important:: **Behavior change** When searching for a contact, the search has to be at least two characters

**XiVO Edge**

* `#3752 <https://projects.xivo.solutions/issues/3752>`_ - Be able to authenticate webRTC user with the TURN server
* `#3871 <https://projects.xivo.solutions/issues/3871>`_ - Edge proxy - Token format check and websocket mds protection
* `#3872 <https://projects.xivo.solutions/issues/3872>`_ - Edge proxy - Split login page from the rest
* `#3884 <https://projects.xivo.solutions/issues/3884>`_ - Be able to route sip request via Kamailio
* `#3898 <https://projects.xivo.solutions/issues/3898>`_ - Configure coTurn for TURN with authentication

    .. important:: **Behavior change** Setting a STUN server in General Configuration / SIP will broke WebRTC.

**XiVO PBX**

* `#3694 <https://projects.xivo.solutions/issues/3694>`_ - XDS - Display the Media Server display name in the users list
* `#3883 <https://projects.xivo.solutions/issues/3883>`_ - Fix jitter buffer implementation mis-spelling to be able to configure it as adaptive


2021.04
=======

Consult the `2021.04 Roadmap <https://projects.xivo.solutions/versions/241>`_.

Components updated: **config-mgt**, **sipml5-xivo-mirror**, **xivo-confgend**, **xivo-config**, **xivo-sysconfd**, **xivo-web-interface**, **xucmgt**, **xucserver**

**CCAgent**

* `#3843 <https://projects.xivo.solutions/issues/3843>`_ - CC agent Ringtone choice is displayed even if we don't have webrtc line

**Desktop Assistant**

* `#3849 <https://projects.xivo.solutions/issues/3849>`_ - UC Assistant - callto tag with E164 numbers fails

**Web Assistant**

* `#3850 <https://projects.xivo.solutions/issues/3850>`_ - Desktop assistant links are no more displayed

**WebRTC**

* `#3751 <https://projects.xivo.solutions/issues/3751>`_ - Installation and configuration server STUN / TURN
* `#3753 <https://projects.xivo.solutions/issues/3753>`_ - Propagate STUN/TURN configuration to a webRTC endpoint
* `#3820 <https://projects.xivo.solutions/issues/3820>`_ - WebRTC Access - Web Proxy
* `#3842 <https://projects.xivo.solutions/issues/3842>`_ - Be able to skip/shorten the ICE candidate gathering
* `#3844 <https://projects.xivo.solutions/issues/3844>`_ - Confgend : Be able to configure RTP conf with STUN server address
* `#3845 <https://projects.xivo.solutions/issues/3845>`_ - Webi : Be able to send message to bus when saving sip configuration
* `#3846 <https://projects.xivo.solutions/issues/3846>`_ - Configmgt : Create and API to retrieve Stun address
* `#3847 <https://projects.xivo.solutions/issues/3847>`_ - XUC : Add stun address to line config

**XiVO PBX**

* `#3835 <https://projects.xivo.solutions/issues/3835>`_ - Configure STUN server in XiVO/XiVO CC
* `#3851 <https://projects.xivo.solutions/issues/3851>`_ - XDS Sync - Script should not run on MDS
* `#3873 <https://projects.xivo.solutions/issues/3873>`_ - [C] - UA users are not listed in the Call permission menu


2021.03
=======

Consult the `2021.03 Roadmap <https://projects.xivo.solutions/versions/239>`_.

Components updated: **recording-server**, **sipml5-xivo-mirror**, **xivo-config**, **xivo-install-script**, **xivo-webi-nginx**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCManager**

* `#3830 <https://projects.xivo.solutions/issues/3830>`_ - [Doc] Correct documentation for ENFORCE_MANAGER_SECURITY option for xucmgt

**Recording**

* `#3724 <https://projects.xivo.solutions/issues/3724>`_ - [C] - Recorded files are not seekable under Chrome browser
* `#3758 <https://projects.xivo.solutions/issues/3758>`_ - Recording download fails in Chrome

**WebRTC**

* `#3691 <https://projects.xivo.solutions/issues/3691>`_ - As a User I want to be warned if the application detects bad network condition
* `#3725 <https://projects.xivo.solutions/issues/3725>`_ - Upgrade xucmgt components - frontend
* `#3796 <https://projects.xivo.solutions/issues/3796>`_ - Add the audio quality detection feature to CCAgent app
* `#3799 <https://projects.xivo.solutions/issues/3799>`_ - As an user I may want to see advanced statistics about webrtc audio quality
* `#3814 <https://projects.xivo.solutions/issues/3814>`_ - Add the down part of webrtc statistics to the ucassistant and ccagent

**XUC Server**

* `#3762 <https://projects.xivo.solutions/issues/3762>`_ - XUC - Change hint status on asterisk when a user DND or forwards are changed

**XiVO PBX**

* `#2650 <https://projects.xivo.solutions/issues/2650>`_ - XDS - Sync files from Main to MDS

2021.02
=======

Consult the `2021.02 Roadmap <https://projects.xivo.solutions/versions/237>`_.

Components updated: **asterisk**, **config-mgt**, **recording-server**, **xivo-confgend**, **xivo-config**, **xivo-web-interface**, **xucmgt**

**Asterisk**

* `#3755 <https://projects.xivo.solutions/issues/3755>`_ - Build asterisk with opus codec

**Chat**

* `#3795 <https://projects.xivo.solutions/issues/3795>`_ - Call button in conversation disappears when sending first message

**Config mgt**

* `#3761 <https://projects.xivo.solutions/issues/3761>`_ - Forward & DND user API in configmgt

**Reporting**

* `#3701 <https://projects.xivo.solutions/issues/3701>`_ - Check ELK upgrade impact on custom Kibana dashboard and Elastic data
* `#3807 <https://projects.xivo.solutions/issues/3807>`_ - [C] - Lots of agent history request can load the reporting server

  .. important:: **Behavior change** :ref:`agent_agent_history` is now limited to the last 7 days: it displays the last 20 calls for the last 7 days period
        (previously it was displaying the last 20 calls with no period limit - which could overload the Reporting Server)


**WebRTC**

* `#3756 <https://projects.xivo.solutions/issues/3756>`_ - Microphone Muted detection only works for first call

**XiVO PBX**

* `#3651 <https://projects.xivo.solutions/issues/3651>`_ - Support the opus codec

  .. important:: **Behavior change**

    Default codecs for Webrtc lines (also for Webrtc line of Unique Account user):

    - changed to: **opus, alaw, vp8**
    - only these codecs are used for these line types

    Lines with customized codecs:

    - Phone and Webrtc lines use only the chosen codecs
    - for Unique Account user, codec customization only applies to the Webrtc line

* `#3750 <https://projects.xivo.solutions/issues/3750>`_ - Periodic cleaning of the personal contacts not linked to any existing user


2021.01
=======

Consult the `2021.01 Roadmap <https://projects.xivo.solutions/versions/234>`_.

Components updated: **pack-reporting**, **packaging**, **recording-server**, **xivo-db-replication**, **xivo-full-stats**, **xivo-lib-python**, **xivo-monitoring**, **xivo-outcall**, **xivo-switchboard-reports**, **xivo-webi-nginx**, **xivocc-installer**, **xucmgt**, **xucserver**

**Desktop Assistant**

* `#3613 <https://projects.xivo.solutions/issues/3613>`_ - error appears when opening devtools on loading error page

**Recording**

* `#3723 <https://projects.xivo.solutions/issues/3723>`_ - Doc - API doesn't behave the same when searching for all recordings or searching a specific call
* `#3726 <https://projects.xivo.solutions/issues/3726>`_ - Upgrade recording-server components
* `#3743 <https://projects.xivo.solutions/issues/3743>`_ - As a recording user I want to be able to search a recording with part of the number

**Reporting**

* `#3747 <https://projects.xivo.solutions/issues/3747>`_ - stats - hangup_time for an incoming call to a queue transferred to another queue is sometime missing in call_on_queue

**Web Assistant**

* `#3728 <https://projects.xivo.solutions/issues/3728>`_ - As a UC/CCAgent/Switchboard user I want to be able to send a predefined email to a search results
* `#3729 <https://projects.xivo.solutions/issues/3729>`_ - I can get the defined email template through a xucmgt endpoint
* `#3730 <https://projects.xivo.solutions/issues/3730>`_ - As a user I can open (from the search result) an email (in my email client) prefilled with defined template and with callernum/dstname prefilled

**WebRTC**

* `#3684 <https://projects.xivo.solutions/issues/3684>`_ - Nginx upgrade on XiVO
* `#3690 <https://projects.xivo.solutions/issues/3690>`_ - As a User I want to be warned if mic generates no noise

**XiVO PBX**

* `#3749 <https://projects.xivo.solutions/issues/3749>`_ - Asterisk codec graphic doesn't work anymore
* `#3754 <https://projects.xivo.solutions/issues/3754>`_ - Wizard error when domain begin by a number

  .. important:: **Behavior change** Wizard is less strict on domain name check: particularly it now accepts a digit as the first letter of a domain part.


**XiVOCC Infra**

* `#3727 <https://projects.xivo.solutions/issues/3727>`_ - Update Java openjdk docker images for scala applications


2020.21
=======

Consult the `2020.21 Roadmap <https://projects.xivo.solutions/versions/233>`_.

Components updated: **config-mgt**, **recording-server**, **xivo-config**, **xivo-db**, **xivo-doc-devices**, **xivo-outcall**, **xivo-provd-plugins**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#3638 <https://projects.xivo.solutions/issues/3638>`_ - Customer call history show call answered by an agent while call was abandonned (Front End Fix)

**Chat**

* `#3670 <https://projects.xivo.solutions/issues/3670>`_ - Upgrade xuc components

**GCU**

* `#3326 <https://projects.xivo.solutions/issues/3326>`_ - XCU : Bug in 2018.04 with context name
* `#3327 <https://projects.xivo.solutions/issues/3327>`_ - XCU : Bug with route_metadata

**Recording**

* `#3710 <https://projects.xivo.solutions/issues/3710>`_ - [C] - Recording server - purge don't work when high number of file

**Web Assistant**

* `#3606 <https://projects.xivo.solutions/issues/3606>`_ - UI to invite someone in conference
* `#3659 <https://projects.xivo.solutions/issues/3659>`_ - UI - Conference with long name does not fit
* `#3668 <https://projects.xivo.solutions/issues/3668>`_ - Add the possibility to hide download Windows/Linux on UC Assistant/CCAgent
* `#3692 <https://projects.xivo.solutions/issues/3692>`_ - As a conference administrator I can invite someone from its extension in the current conference

  .. important:: **Behavior change** When in conference and typing a phone number in the searchbar, a dropdown menu will show with 3 options :
    - Call
    - Invite to conference
    - Search

    It is now possible to search by phone numbers when in a conference. To enable it, modify your directories definition.


**WebRTC**

* `#3671 <https://projects.xivo.solutions/issues/3671>`_ - Upgrade xucmgt components - play/scala
* `#3702 <https://projects.xivo.solutions/issues/3702>`_ - Upgrade config-mgt components

**XiVO PBX**

* `#3058 <https://projects.xivo.solutions/issues/3058>`_ - XiVO containers are not upgraded after docker-ce upgrade
* `#3649 <https://projects.xivo.solutions/issues/3649>`_ - [C] - Outcall - nb of connection to the database
* `#3705 <https://projects.xivo.solutions/issues/3705>`_ - As XiVO admin of entity B I see users of entity A in users list
* `#3707 <https://projects.xivo.solutions/issues/3707>`_ - [C] - Updating the line template breaks the provd config (it removes the sip line info from the configs)
* `#3712 <https://projects.xivo.solutions/issues/3712>`_ - [Wizard] Default configuration for France creates outcall routes with context "to-extern"
* `#3721 <https://projects.xivo.solutions/issues/3721>`_ - Outcall don't keep "Internal" on upgrade

**XiVO Provisioning**

* `#3720 <https://projects.xivo.solutions/issues/3720>`_ - Yealink firmware v85 broken link for T31/T33
* `#3722 <https://projects.xivo.solutions/issues/3722>`_ - Yealink : add new yealink MAC adresses prefix 80:5E:0C


2020.20
=======

Consult the `2020.20 Roadmap <https://projects.xivo.solutions/versions/232>`_.

Components updated: **config-mgt**, **logstash**, **recording-server**, **xivo-web-interface**, **xivocc-installer**, **xivoxc-nginx**, **xucmgt**, **xucserver**

**Config mgt**

* `#2762 <https://projects.xivo.solutions/issues/2762>`_ - S - Be able to manually change play auth token of our application (configmgt, recording ...)

**Desktop Assistant**

* `#3641 <https://projects.xivo.solutions/issues/3641>`_ - Update Electron to the very latest version

**Reporting**

* `#3666 <https://projects.xivo.solutions/issues/3666>`_ - Update Kibana (ELK stack) to last version

**Security**

* `#3661 <https://projects.xivo.solutions/issues/3661>`_ - Upgrade docker - test and fix/document upgrade from xivo/cc version on Stretch debian

**Web Assistant**

* `#3678 <https://projects.xivo.solutions/issues/3678>`_ - As a conference administrator I can invite someone (from search result or favorites) in the current conference

**WebRTC**

* `#3672 <https://projects.xivo.solutions/issues/3672>`_ - Nginx upgrade on XiVO CC

**XUC Server**

* `#3680 <https://projects.xivo.solutions/issues/3680>`_ - Cannot use Cti.dial
* `#3689 <https://projects.xivo.solutions/issues/3689>`_ - OpenID authentication doesn't work when token has single audience

**XiVO PBX**

* `#3693 <https://projects.xivo.solutions/issues/3693>`_ - Web interface - Protocol Type - Missing "n" to "Personalisé"

**XiVO Provisioning**

* `#3619 <https://projects.xivo.solutions/issues/3619>`_ - Support Yealink firmware V85 for phone : T31P / T33G / T53 / T54W


2020.19
=======

Consult the `2020.19 Roadmap <https://projects.xivo.solutions/versions/229>`_.

Components updated: **rabbitmq**, **xivo-dist**, **xivo-web-interface**, **xucmgt**, **xucserver**


.. warning:: Do not upgrade to this version for a XiVO below Freya.
   Docker upgrade was not tested for XiVO below Freya.

**Chat**

* `#3657 <https://projects.xivo.solutions/issues/3657>`_ - Chat Refactor - 3 - Xuc API Gaia

**Security**

* `#2449 <https://projects.xivo.solutions/issues/2449>`_ - [S] Upgrade docker to latest 19.03 version (and docker-compose to  1.27.4)

  .. important:: Docker is upgraded to 19.03.13 and docker-compose to 1.27.4.

**XiVO PBX**

* `#3646 <https://projects.xivo.solutions/issues/3646>`_ - [C] - Order function key listing in user form list
* `#3647 <https://projects.xivo.solutions/issues/3647>`_ - Queue exit context can't be set if there is no Service type of context
* `#3648 <https://projects.xivo.solutions/issues/3648>`_ - Rabbitmq can't be built because of missing plugin source
